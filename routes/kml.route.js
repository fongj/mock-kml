const fs = require('fs');
const request = require('request');
const argv = require('yargs').argv;
const basicAuth = require('express-basic-auth');

const port = argv.port || 3001;

module.exports.init = (app, origin) => {
  const setContentTypeKml = (req, res, next) => {
    res.set('Content-Type', 'application/vnd.google-earth.kml+xml');
    next();
  };

  const setContentTypeKmz = (req, res, next) => {
    res.set('Content-Type', 'application/vnd.google-earth.kmz');
    next();
  };

  const allowCORS = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', req.header('origin'));
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
  };

  const handleOptions = (req, res, next) => {
      if ('OPTIONS' === req.method) {
          res.sendStatus(200);
      } else {
          next();
      }
  };
 
  app.use(basicAuth({
      users: { 'admin': 'secret' }
  }))

  app.get('/kml', allowCORS, handleOptions, setContentTypeKml, (req, res, next) => {
    const kml = fs.readFileSync('data/1.kml', 'utf8');
    return res.send(eval('`' + kml + '`'));
  });

  app.get('/kmz', allowCORS, handleOptions, setContentTypeKmz, (req, res, next) => {
    const kmz = fs.readFileSync('data/1.kmz');
    return res.send(kmz);
  });
  
  app.get('/networklink', allowCORS, handleOptions, setContentTypeKml, (req, res, next) => {
    return res.send(`
      <kml xmlns="http://www.opengis.net/kml/2.2">
        <NetworkLink>
          <name>NL 4</name>
          <refreshVisibility>0</refreshVisibility>
          <flyToView>0</flyToView>
          <Link>
            <href>http://localhost:${port}/kml</href>
            <refreshMode>onInterval</refreshMode>
            <refreshInterval>5</refreshInterval>
            <viewFormat>BBOX=[bboxWest],[bboxSouth],[bboxEast],[bboxNorth]</viewFormat>
          </Link>
        </NetworkLink>
      </kml>
    `);
  });
}
