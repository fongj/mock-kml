const express = require('express');
const argv = require('yargs').argv;

const port = argv.port || 3001;
const origin = argv.origin || 'http://localhost:3000';
const app = express();

require('./routes').init(app, origin);

app.listen(port);
console.log('Now listening on port [%s]', port);

process.on('uncaughtException', (err) => {
    console.log(err);
});
