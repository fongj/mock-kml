# Mock KML

## Setup:
```npm install```

## Usage:
```npm start -- --port 3002 --origin http://localhost:3000```

## Debugging:
```npm run debug -- --port 3002 --origin http://localhost:3000```

## Options:
* origin is defaulted to `http://localhost:3000`. This value goes into the CORS header.
* port is defaulted to `3001`

## Testing SIRIS WebSockets
### Mock KML
1. clone the mock-kml repo
2. start it up: `npm start`
3. it should now be running on 3001

### Siris WebSockets
1. clone the siris-websockets repo
2. ensure `disableOriginRestriction` is set to true in config
3. start it up: `npm run dev`
4. it should be running on 3003

### Browser
1. Open http://localhost:3003/static/kml/?zoom=7&latLng=0,0&load=true&url=http://localhost:3001/networklink/

At this point, the KML will be updated on a timer. You can now go into mock-kml/data directory and modify `1.kml`. Any changes will now reflect live in the browser.